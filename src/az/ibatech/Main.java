package az.ibatech;

import az.ibatech.controller.FamilyController;
import az.ibatech.model.Dog;
import az.ibatech.model.Man;
import az.ibatech.model.Woman;

public class Main {

    private final static FamilyController familyController = new FamilyController();

    public static void main(String[] args) {
        Woman mother = new Woman("Rahima", "Qasimova", 2000);
        Man father = new Man("Kerem", "Qasimov", 2001);
        father.repairCar();
        mother.makeUp();

        familyController.createNewFamily(father, mother);
        mother.setFamily(familyController.getFamilyById(0));
        father.setFamily(familyController.getFamilyById(0));

        Woman esma = new Woman("Esma", "Ahmadov", 2020, 160);

        esma.setSchedule("Monday", "go to courses; watch a film");
        esma.setSchedule("Sunday", "do home work");
        esma.setSchedule("Wednesday", "do workout");
        esma.setSchedule("Friday", "read e-mails");
        esma.setSchedule("Saturday", "do shopping");
        esma.setSchedule("Thursday", "visit grandparents");
        esma.setSchedule("Tuesday", "do household");

        familyController.getFamilyById(0).addChild(esma);
        esma.setFamily(familyController.getFamilyById(0));

        Dog esmasPet = new Dog("Rex", 5, 49);
        esmasPet.setHabits("eat");
        esmasPet.setHabits("drink");
        esmasPet.setHabits("sleep");
        familyController.getFamilyById(0).setPet(esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();

        System.out.println(esmasPet.toString());

        System.out.println(esma.toString());

        Man child1 = new Man();
        Woman child2 = new Woman();
        Man child3 = new Man();
        familyController.getFamilyById(0).addChild(child1);
        familyController.getFamilyById(0).addChild(child2);
        familyController.getFamilyById(0).addChild(child3);
        familyController.getFamilyById(0).countFamily();

        System.out.println();

        familyController.getFamilyById(0).deleteChild(2);
        familyController.getFamilyById(0).countFamily();

        System.out.println();

        familyController.getFamilyById(0).deleteChild(child3);
        familyController.getFamilyById(0).countFamily();
    }
}

