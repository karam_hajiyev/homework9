package az.ibatech.controller;

import az.ibatech.model.Family;
import az.ibatech.model.Human;
import az.ibatech.model.Pet;
import az.ibatech.service.FamilyService;
import az.ibatech.service.impl.FamilyServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class FamilyController {
    private final FamilyService familyService;

    public FamilyController() {
        this.familyService = new FamilyServiceImpl();
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int count) {
        familyService.getFamiliesBiggerThan(count);
    }

    public void getFamiliesLessThan(int count) {
        familyService.getFamiliesLessThan(count);
    }

    public void countFamiliesWithMemberNumber(int count) {
        familyService.countFamiliesWithMemberNumber(count);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public void bornChild(int index, Human child) {
        familyService.bornChild(index, child);
    }

    public void adoptChild(int index, ArrayList<Human> child) {
        familyService.adoptChild(index, child);
    }

    public void deleteAllChildrenOlderThen(int index, Human child) {
        familyService.deleteAllChildrenOlderThen(index, child);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Pet getPets() {
        return familyService.getPets();
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }
}
