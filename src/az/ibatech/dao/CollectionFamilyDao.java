package az.ibatech.dao;

import az.ibatech.model.Family;

import java.util.List;

public interface CollectionFamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int id);

    boolean deleteFamily(int id);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);
}

