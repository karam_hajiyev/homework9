package az.ibatech.dao.impl;

import az.ibatech.model.Family;
import az.ibatech.dao.CollectionFamilyDao;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDaoImpl implements CollectionFamilyDao {
    private final List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int id) {
        if (id > families.size() || id < 0) return null;
        return families.get(id);
    }

    @Override
    public boolean deleteFamily(int id) {
        if (id > families.size() || id < 0) return false;
        families.remove(id);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (!families.contains(family)) return false;
        families.remove(family);
        return true;
    }

    @Override
    public void saveFamily(Family family) {
        this.families.add(family);
    }
}
