package az.ibatech.model;

public class DomesticCat extends Pet {
    // constructors
    public DomesticCat() {
    }

    public DomesticCat(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.DOMESTICCAT;
    }

    // methods
    public void foul() {
        System.out.println("I need to cover it up.");
    }
}