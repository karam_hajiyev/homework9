package az.ibatech.model;

import az.ibatech.model.Human;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name,surname,year);
    }

    public Man(String name, String surname, int year, int iq) {
        super(name,surname,year,iq);

    }

    public void repairCar() {
        System.out.println("It's time to repair car.");
    }

    public void greetPet() {
        System.out.println("Hello, " + getFamily().getPet().getNickname() + '.');
    }
}
