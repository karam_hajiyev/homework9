package az.ibatech.model;

public enum Species {
    DOG,
    DOMESTICCAT,
    ROBOCAT,
    FISH,
    UNKNOWN
}

