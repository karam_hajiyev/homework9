package az.ibatech.model;

import az.ibatech.model.Human;

public final class Woman extends Human {
    // constructors
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year);
    }

    public void makeUp() {
        System.out.println("It's time to go to the beautician.");
    }

    public void greetPet() {
        System.out.println("Hello, my sweeties " + getFamily().getPet().getNickname() + '.');
    }
}
