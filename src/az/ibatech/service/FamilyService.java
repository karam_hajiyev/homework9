package az.ibatech.service;

import az.ibatech.model.Family;
import az.ibatech.model.Human;
import az.ibatech.model.Pet;

import java.util.ArrayList;
import java.util.List;

public interface FamilyService {
    void saveFamily(Family family);

    List<Family> getAllFamilies();

    void displayAllFamilies();

    void getFamiliesBiggerThan(int count);

    void getFamiliesLessThan(int count);

    void countFamiliesWithMemberNumber(int count);

    void createNewFamily(Human father, Human mother);

    void deleteFamilyByIndex(int index);

    void bornChild(int index, Human child);

    void adoptChild(int index, ArrayList<Human> child);

    void deleteAllChildrenOlderThen(int index, Human child);

    int count();

    Family getFamilyById(int index);

    Pet getPets();

    void addPet(int index, Pet pet);
}
