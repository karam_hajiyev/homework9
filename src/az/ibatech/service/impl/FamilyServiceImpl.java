package az.ibatech.service.impl;

import az.ibatech.dao.CollectionFamilyDao;
import az.ibatech.dao.impl.CollectionFamilyDaoImpl;
import az.ibatech.model.Dog;
import az.ibatech.model.Family;
import az.ibatech.model.Human;
import az.ibatech.model.Pet;
import az.ibatech.service.FamilyService;

import java.util.ArrayList;
import java.util.List;

public class FamilyServiceImpl implements FamilyService {
    private final CollectionFamilyDao collectionFamilyDao;

    public FamilyServiceImpl() {
        this.collectionFamilyDao = new CollectionFamilyDaoImpl();
    }

    public void saveFamily(Family family) {
        collectionFamilyDao.saveFamily(family);
    }

    public List<Family> getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        int index = 0;
        for (Family i : familyList) {
            System.out.println("Index: " + index + "\t" + "Family: " + i.toString());
            index++;
        }
    }

    public void getFamiliesBiggerThan(int count) {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        int index = 0;
        for (Family i : familyList) {
            if (count < i.countFamily())
                System.out.println("Index: " + index + "\t" + "Family: " + i.toString());
            index++;
        }
    }

    public void getFamiliesLessThan(int count) {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        int index = 0;
        for (Family i : familyList) {
            if (count > i.countFamily())
                System.out.println("Index: " + index + "\t" + "Family: " + i.toString());
            index++;
        }
    }

    public void countFamiliesWithMemberNumber(int count) {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        int index = 0;
        for (Family i : familyList)
            if (count == i.countFamily())
                index++;
        System.out.println(index);
    }

    public void createNewFamily(Human father, Human mother) {
        Family newFamily = new Family(mother, father);
        collectionFamilyDao.saveFamily(newFamily);
    }

    public void deleteFamilyByIndex(int index) {
        collectionFamilyDao.deleteFamily(index);
    }

    public void bornChild(int index, Human child) {
        collectionFamilyDao.getFamilyByIndex(index).addChild(child);
    }

    public void adoptChild(int index, ArrayList<Human> child) {
        collectionFamilyDao.getFamilyByIndex(index).setChildren(child);
    }

    public void deleteAllChildrenOlderThen(int index, Human child) {
        collectionFamilyDao.getFamilyByIndex(index).deleteChild(child);
    }

    public int count() {
        return collectionFamilyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return collectionFamilyDao.getFamilyByIndex(index);
    }

    public Pet getPets() {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        //List<Objects> petList = new List<Dog>();
        Pet pet = new Dog();
        for (Family i : familyList) {
            pet = i.getPet();
        }
        return pet;
    }

    public void addPet(int index, Pet pet) {
        collectionFamilyDao.getFamilyByIndex(index).setPet(pet);
    }

}
